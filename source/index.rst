##########
Bienvenido
##########

Esta es la documetación del proyecto **Autito**. El proyecto consiste en el desarrollo de un auto a control remoto equipado con multiples sensores y con envio de telemetria.

====
Auto
====

El auto consiste en un chasis de acrílico con 4 ruedas de tracción individual en cada una. Controlado por un STM32, la comunicación de control y telemetría se realiza mediante XBEE por comandos AT.

===========
Controlador
===========

El controlador y receptor de la telemetría del autito es una Raspberry Pi Zero con bateria conectada a una pantalla LCD táctil.

======
Partes
======

- `STM32 <https://articulo.mercadolibre.com.ar/MLA-1433224194-modulo-para-desarrollo-stm32f103c8t6-arm-compatible-arduino-_JM>`_
- `ST-Link <https://articulo.mercadolibre.com.ar/MLA-818524189-programador-usb-st-link-v2-stm8-stm32-usb-con-cable-_JM>`_
- `XBEE <https://articulo.mercadolibre.com.ar/MLA-1424547582-modulo-xbee-xb24cz7sit-004-_JM>`_
- `Adaptador XBEE-USB <https://articulo.mercadolibre.com.ar/MLA-1160883288-adaptador-interfaz-usb-xbee-driver-ftdi-ft232rl-5v-_JM>`_
- `Raspberry Pi Zero <https://articulo.mercadolibre.com.ar/MLA-1381189652-raspberry-pi-zero-2w-original-made-in-uk-_JM>`_
- `Cargador de baterias <https://articulo.mercadolibre.com.ar/MLA-778953661-adafruit-pid-2030-powerboost-1000-basic-5v-usb-boost-1000ma-_JM>`_
- `Conector de baterias 4 paralelo <https://articulo.mercadolibre.com.ar/MLA-1401339490-porta-pila-bateria-x4-18650-cuatro-baterias-en-paralelo-hobb-_JM>`_
- `Pantalla de 7' <https://articulo.mercadolibre.com.ar/MLA-818229291-pantalla-tactil-raspberry-pi-lcd-touch-7-1024-x-600-hdmi-_JM>`_
- `Baterias 18650 <https://www.mercadolibre.com.ar/bateria-litio-recargable-2200ma-37v-luz-emergencia-macroled/p/MLA20014548>`_
- `Chasis <https://articulo.mercadolibre.com.ar/MLA-621979120-kit-chasis-auto-robot-4wd-4-motores-rover-arduino-ptec-_JM>`_
